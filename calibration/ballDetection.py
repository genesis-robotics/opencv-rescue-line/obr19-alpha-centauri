import numpy as np
import cv2
import math
import const

if const.ON_RPI_CAM:
    from RaspiCam import RaspiCam as Camera
else:
    from USBCam import USBCam as Camera

capture = Camera(const.FRAME_WIDTH, const.FRAME_HEIGHT).start()

GAUSSIAN_BLUR_VALUE = 19    # 13 para 640/480
BGR_VALUE = 100

# Parametros para HoughCircles
CIRCLE_DP_PARAM = 1.6
CIRCLE_DBETWEEN_MIN = int(70 / const.PX_DIVISOR)
CIRCLE_RADIUS_MIN = 0
CIRCLE_RADIUS_MAX = 0
CIRCLE_PARAM2 = 65


def callback(x):
    pass


cv2.namedWindow("image")
cv2.createTrackbar("BGR_value", "image", BGR_VALUE, 255, callback)
cv2.createTrackbar("dp_param", "image", int(CIRCLE_DP_PARAM*10), 20, callback)
cv2.createTrackbar("min_radius", "image", CIRCLE_RADIUS_MIN, 300, callback)
cv2.createTrackbar("max_radius", "image", CIRCLE_RADIUS_MAX, 300, callback)
cv2.createTrackbar("dbetween", "image", CIRCLE_DBETWEEN_MIN, 200, callback)
cv2.createTrackbar("param2", "image", CIRCLE_PARAM2, 250, callback)
cv2.createTrackbar("blur", "image", GAUSSIAN_BLUR_VALUE, 30, callback)


def getCircles(image):
    bin_img = cv2.inRange(
        image, (BGR_VALUE, BGR_VALUE, BGR_VALUE), (255, 255, 255))
    bin_img = cv2.GaussianBlur(
        bin_img, (GAUSSIAN_BLUR_VALUE, GAUSSIAN_BLUR_VALUE), cv2.BORDER_DEFAULT)
    circles = cv2.HoughCircles(bin_img, cv2.HOUGH_GRADIENT, CIRCLE_DP_PARAM,
                               CIRCLE_DBETWEEN_MIN, param2=CIRCLE_PARAM2, minRadius=CIRCLE_RADIUS_MIN, maxRadius=CIRCLE_RADIUS_MAX)
    if circles is not None:
        circles = np.round(circles[0, :]).astype("int")
    return circles, bin_img


if __name__ == "__main__":
    while True:
        frame = capture.getFrame()
        if not len(frame):
            continue

        BGR_VALUE = cv2.getTrackbarPos("BGR_value", "image")
        CIRCLE_DP_PARAM = cv2.getTrackbarPos("dp_param", "image") / 10
        CIRCLE_RADIUS_MIN = cv2.getTrackbarPos("min_radius", "image")
        CIRCLE_RADIUS_MAX = cv2.getTrackbarPos("max_radius", "image")
        CIRCLE_DBETWEEN_MIN = cv2.getTrackbarPos("dbetween", "image")
        GAUSSIAN_BLUR_VALUE = cv2.getTrackbarPos("blur", "image")
        CIRCLE_PARAM2 = cv2.getTrackbarPos("param2", "image")

        if GAUSSIAN_BLUR_VALUE % 2 == 0:
            GAUSSIAN_BLUR_VALUE = GAUSSIAN_BLUR_VALUE - 1

        circles, img = getCircles(frame)

        if circles is not None:
            for (x, y, r) in circles:

                cv2.circle(frame, (x, y), r, (0, 255, 0), 4)
                cv2.rectangle(frame, (x - 5, y - 5),
                              (x + 5, y + 5), (0, 128, 255), -1)

        cv2.imshow("binary", img)
        cv2.imshow("image", frame)
        if cv2.waitKey(1) & 0xFF == ord("q"):
            break

capture.stop()
cv2.destroyAllWindows()

print()
print("MAX BGR: "+str((BGR_VALUE, BGR_VALUE, BGR_VALUE)))
print("DP param: "+str(CIRCLE_DP_PARAM) +
      "  Distance between: "+str(CIRCLE_DBETWEEN_MIN))
print("RADIUS - min: "+str(CIRCLE_RADIUS_MIN)+" - max: "+str(CIRCLE_RADIUS_MAX))
print("Blur Value: "+str(GAUSSIAN_BLUR_VALUE))
print("Param2: "+str(CIRCLE_PARAM2))
