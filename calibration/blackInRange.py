import cv2
import numpy as np
import const

if const.ON_RPI_CAM:
    from RaspiCam import RaspiCam as Camera
else:
    from USBCam import USBCam as Camera

capture = Camera(const.FRAME_WIDTH, const.FRAME_HEIGHT).start()

erodeElement = np.ones((3, 3), np.uint8)


def callback(x):
    pass


min_B = None
max_B = None
min_G = None
max_G = None
min_R = None
max_R = None
n_iterations = None
min_area = None

BLACK_ERODE_KERNEL = np.ones((3, 3), np.uint8)

cv2.namedWindow("frame")
cv2.createTrackbar("min_B", "frame", 0, 256, callback)
cv2.createTrackbar("max_B", "frame", 85, 256, callback)
cv2.createTrackbar("min_G", "frame", 0, 256, callback)
cv2.createTrackbar("max_G", "frame", 70, 256, callback)
cv2.createTrackbar("min_R", "frame", 0, 256, callback)
cv2.createTrackbar("max_R", "frame", 80, 256, callback)
cv2.createTrackbar("iterations", "frame", 2, 15, callback)
cv2.createTrackbar("min_area", "frame", 3000, 35000, callback)

while True:
    frame = capture.getFrame()
    if not len(frame):
        continue

    min_B = cv2.getTrackbarPos("min_B", "frame")
    max_B = cv2.getTrackbarPos("max_B", "frame")
    min_G = cv2.getTrackbarPos("min_G", "frame")
    max_G = cv2.getTrackbarPos("max_G", "frame")
    min_R = cv2.getTrackbarPos("min_R", "frame")
    max_R = cv2.getTrackbarPos("max_R", "frame")
    n_iterations = cv2.getTrackbarPos("iterations", "frame")
    min_area = cv2.getTrackbarPos("min_area", "frame")

    blackline = cv2.inRange(
        frame, (min_B, min_G, min_R), (max_B, max_G, max_R))
    blackline = cv2.erode(blackline, BLACK_ERODE_KERNEL,
                          iterations=n_iterations)

    contours, _ = cv2.findContours(
        blackline.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    new_contours = []

    for contour in contours:
        moment = cv2.moments(contour)
        if moment["m00"] >= min_area:
            new_contours.append(contour)

    frame = cv2.drawContours(frame, new_contours, -1, (0, 255, 0), 2)

    cv2.imshow("frame", frame)

    if cv2.waitKey(1) & 0xFF == ord("q"):
        break

print()
print("Minimum BGR: ("+str(min_B)+", "+str(min_G)+", "+str(min_R)+")")
print("Maximum BGR: ("+str(max_B)+", "+str(max_G)+", "+str(max_R)+")")
print("Number of iterations: "+str(n_iterations))
print("Minimum area: "+str(min_area))

capture.stop()
cv2.destroyAllWindows()
