import cv2
import numpy as np
import const

if const.ON_RPI_CAM:
    from RaspiCam import RaspiCam as Camera
else:
    from USBCam import USBCam as Camera

capture = Camera(const.FRAME_WIDTH, const.FRAME_HEIGHT).start()

erodeElement = np.ones((3, 3), np.uint8)


def callback(x):
    pass


min_H = None
max_H = None
min_S = None
max_S = None
min_V = None
max_V = None
n_iterations = None
min_area = None

cv2.namedWindow("frame")
cv2.createTrackbar("min_H", "frame", 31, 256, callback)
cv2.createTrackbar("max_H", "frame", 74, 256, callback)
cv2.createTrackbar("min_S", "frame", 133, 256, callback)
cv2.createTrackbar("max_S", "frame", 256, 256, callback)
cv2.createTrackbar("min_V", "frame", 53, 256, callback)
cv2.createTrackbar("max_V", "frame", 256, 256, callback)
cv2.createTrackbar("iterations", "frame", 3, 15, callback)
cv2.createTrackbar("min_area", "frame", 500, 35000, callback)

while True:
    frame = capture.getFrame()
    if not len(frame):
        continue

    min_H = cv2.getTrackbarPos("min_H", "frame")
    max_H = cv2.getTrackbarPos("max_H", "frame")
    min_S = cv2.getTrackbarPos("min_S", "frame")
    max_S = cv2.getTrackbarPos("max_S", "frame")
    min_V = cv2.getTrackbarPos("min_V", "frame")
    max_V = cv2.getTrackbarPos("max_V", "frame")
    n_iterations = cv2.getTrackbarPos("iterations", "frame")
    min_area = cv2.getTrackbarPos("min_area", "frame")

    hsv_image = cv2.cvtColor(frame, cv2.COLOR_RGB2HSV)

    thresh_image = cv2.inRange(
        hsv_image, (min_H, min_S, min_V), (max_H, max_S, max_V))
    thresh_image = cv2.erode(
        thresh_image, erodeElement, iterations=n_iterations)

    contours, _ = cv2.findContours(
        thresh_image, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
    good_contours = []
    bad_contours = []

    for i, contour in enumerate(contours):
        moment = cv2.moments(contour)
        if moment["m00"] >= min_area:
            good_contours.append(contour)
        else:
            bad_contours.append(contour)

    frame = cv2.drawContours(frame, good_contours, -1, (0, 255, 0), 3)
    frame = cv2.drawContours(frame, bad_contours, -1, (0, 0, 255), 3)

    cv2.imshow("frame", frame)

    if cv2.waitKey(1) & 0xFF == ord("q"):
        break

print()
print("Minimum HSV: ("+str(min_H)+", "+str(min_S)+", "+str(min_V)+")")
print("Maximum HSV: ("+str(max_H)+", "+str(max_S)+", "+str(max_V)+")")
print("Number of iterations: "+str(n_iterations))
print("Minimum area: "+str(min_area))

capture.stop()
cv2.destroyAllWindows()
