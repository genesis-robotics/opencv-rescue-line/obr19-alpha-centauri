import time
import math
import cv2
import numpy as np
import const

if const.ON_RPI_CAM:
    from RaspiCam import RaspiCam as Camera
else:
    from USBCam import USBCam as Camera

capture = Camera(const.FRAME_WIDTH, const.FRAME_HEIGHT).start()

PX_DIVISOR = const.PX_DIVISOR

BLACK_BGR_MINIMUM = (0, 0, 0)
BLACK_BGR_MAXIMUM = (60, 60, 60)
BLACK_ERODE_KERNEL = np.ones((3, 3), np.uint8)
BLACK_ERODE_ITERATIONS = int(2 / PX_DIVISOR)

PIXEL_TOLERANCE = int(8 / PX_DIVISOR)

LINES_DIFFERENCE_MAX_X = int(100 / PX_DIVISOR)


def linesDifference(this_line, last_line):
    this_start = this_line[0] - int(this_line[1]/2)
    this_end = this_line[0] + int(this_line[1]/2)

    last_start = last_line[0] - int(last_line[1]/2)
    last_end = last_line[0] + int(last_line[1]/2)

    if this_end < last_start:
        return last_start - this_end
    elif last_end < this_start:
        return this_start - last_end
    else:
        return 0


def callback(x):
    pass


def intersectionRowScan(row, compare_x, compare_w):
    start = None
    end = None
    tolerance = 0
    lines = []
    foundLine = False

    for i in range(len(row)):
        if row[i]:
            if not start:
                start = i

            tolerance = PIXEL_TOLERANCE + 1
            end = i

        elif start and tolerance:
            tolerance = tolerance - 1

        if start and ((not tolerance and not row[i]) or i == (len(row)-1)):
            if end - start >= MINIMUM_LINE_WIDTH:
                line_x = int(round((start+end)/2))
                line_w = end - start + 1
                lines.append((abs(compare_x - line_x), line_x, line_w))
                foundLine = True

            end = None
            start = None

    if foundLine:
        line = min(lines)
        line = line[:][1:]
        if linesDifference(line, (compare_x, compare_w)) <= LINES_DIFFERENCE_MAX_X:
            return line
    return False, False


MINIMUM_LINE_WIDTH = None
MAXIMUM_LINE_WIDTH = None

cv2.namedWindow("frame")
cv2.createTrackbar("min_width", "frame", 52, 320, callback)
cv2.createTrackbar("max_width", "frame", 150, 640, callback)

last_line = [int(const.FRAME_WIDTH/2), const.FRAME_WIDTH]

while True:
    frame = capture.getFrame()
    if not len(frame):
        continue

    blackline = cv2.inRange(frame, BLACK_BGR_MINIMUM, BLACK_BGR_MAXIMUM)
    blackline = cv2.erode(blackline, BLACK_ERODE_KERNEL,
                          iterations=BLACK_ERODE_ITERATIONS)

    MINIMUM_LINE_WIDTH = cv2.getTrackbarPos("min_width", "frame")
    MAXIMUM_LINE_WIDTH = cv2.getTrackbarPos("max_width", "frame")

    prev_x = last_line[0]
    prev_w = last_line[1]
    line1 = False

    for y in reversed(range(const.FRAME_HEIGHT)):
        line_center, line_width = intersectionRowScan(
            blackline[y][:], prev_x, prev_w)

        if line_width:
            if line_width > MAXIMUM_LINE_WIDTH:
                frame = cv2.line(frame, (line_center - int(line_width/2), y),
                                 (line_center + int(line_width/2), y), (0, 255, 0), 1)
            else:
                frame = cv2.line(frame, (line_center - int(line_width/2), y),
                                 (line_center + int(line_width/2), y), (0, 0, 255), 1)
            prev_x = line_center
            prev_w = line_width
            if not line1:
                line1 = True
                last_line = [prev_x, prev_w]

    cv2.imshow("frame", frame)
    if cv2.waitKey(1) & 0xFF == ord("q"):
        break

print("Max width: "+str(MAXIMUM_LINE_WIDTH))
print("Min width: "+str(MINIMUM_LINE_WIDTH))
capture.stop()
