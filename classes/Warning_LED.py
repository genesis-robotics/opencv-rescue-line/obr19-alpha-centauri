import pigpio
import math


def dec_to_bin(number):
    """Função para transformação de decimal para binario"""
    binary = []
    while number:
        bit = 0 if number % 2 == 0 else 1
        number = (number - bit) / 2
        binary.append(bit)
    binary = binary[::-1]
    return binary


class Warning_LED:
    def __init__(self, pigpio_obj, pins):
        """Iniciar pinos de LED"""
        self.io = pigpio_obj
        self.pins_addr = pins
        for pin in pins:
            self.io.set_mode(pin, pigpio.OUTPUT)
            
        self.reset()
            
    def reset(self):
        for pin in self.pins_addr:
            self.io.write(pin, 0)

    def set(self, warning):
        """Ilumina LEDs conforme o código de aviso"""
        if warning >= math.pow(2, len(self.pins_addr)):
            raise ValueError(
                "Código de aviso excede número de pinos de led disponíveis")

        bin = dec_to_bin(warning)

        if len(bin) < len(self.pins_addr):
            for _ in range(len(self.pins_addr) - len(bin)):
                bin.append(0)

        for i, pin in enumerate(self.pins_addr):
            self.io.write(pin, bin[i])