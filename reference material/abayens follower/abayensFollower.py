from functions import *

capture = cv2.VideoCapture(0)
capture.set(3, FRAME_WIDTH)		# Largura
capture.set(4, FRAME_HEIGHT)		# Altura
capture.set(5, 30)		# FPS
capture.set(10, 100)  # Luminosidade

erodeElement = np.ones((3, 3), np.uint8)

while True:
    _, image = capture.read()

    HSV_image = cv2.cvtColor( image, cv2.COLOR_RGB2HSV )

    thresh_image = cv2.inRange( HSV_image, ( 0, 71, 0 ), ( 87, 256, 256 ))
    eroded_image = cv2.erode( thresh_image, erodeElement, iterations=2)

    greens = trackObject(eroded_image)
    
    if len(greens) > 0:
        drawObject( greens[0][1], greens[0][2], image, ( 0, 255, 0 ))

    cv2.imshow("img", image)

    if cv2.waitKey(1) & 0xFF == ord("q"):
        break

capture.release()
cv2.destroyAllWindows()
