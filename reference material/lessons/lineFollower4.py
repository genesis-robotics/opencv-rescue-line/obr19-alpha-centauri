import time
import cv2
import numpy as np
import tempo

# Cria a instância de captura das imagens da camera
capture = cv2.VideoCapture(0)

# Determina atributos da imagem
capture.set(cv2.CAP_PROP_FRAME_WIDTH, 640)  # Largura
capture.set(cv2.CAP_PROP_FRAME_HEIGHT, 480) # Altura
capture.set(cv2.CAP_PROP_FPS, 30)           # FPS
capture.set(cv2.CAP_PROP_BRIGHTNESS, 100)   # Luminosidade
capture.set(cv2.CAP_PROP_CONTRAST,150)      # Contraste

successful = True
x_last = 320
y_last = 180
fps = tempo.Framerate()

while successful:

    # Captura um frame da camera
    successful, image = capture.read()

    # Seleciona as áreas com cor preta (linha)
    Blackline = cv2.inRange(image, (0,0,0), (75,75,75))

    kernel = np.ones((3,3), np.uint8)

    # Corta rúidos da seleção das cores pretas
    Blackline = cv2.erode(Blackline, kernel, iterations=5)
    Blackline = cv2.dilate(Blackline, kernel, iterations=9)

    # Contorna as áreas de seleção
    contours_blk, hierarchy_blk = cv2.findContours(Blackline.copy(),cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    
    contours_blk_len = len(contours_blk)
    if contours_blk_len > 0 :

        # Caso apenas haja um contorno é desenhado o retangulo
        if contours_blk_len == 1 :
            blackbox = cv2.minAreaRect(contours_blk[0])
        
        # Caso haja mais contornos deve-se descartar os errados
        else:
            canditates=[]
            off_bottom = 0

            # Conta o número de seleções que tocam no limite inferior da imagem
            # e guarda-se algumas informações dessas seleções
            for con_num in range(contours_blk_len):
                blackbox = cv2.minAreaRect(contours_blk[con_num])
                (x_min, y_min), (w_min, h_min), ang = blackbox
                box = cv2.boxPoints(blackbox)
                (x_box,y_box) = box[0]
                if y_box > 478 :
                    off_bottom += 1
                canditates.append((y_box,con_num,x_min,y_min))

            # Ordena o vetor com base no eixo Y do ponto mais baixo na tela
            canditates = sorted(canditates)

            # Caso haja mais de um contorno que toca a parte inferior da tela
            # é selecionado o mais parecido com o da imagem anterior
            if off_bottom > 1:
                canditates_off_bottom=[]
                for con_num in range ((contours_blk_len - off_bottom), contours_blk_len):
                    (y_highest,con_highest,x_min, y_min) = canditates[con_num]

                    # Calcula a distância do centro de cada contorno ao centro do contorno do frame passado
                    total_distance = (abs(x_min - x_last)**2 + abs(y_min - y_last)**2)**0.5
                    canditates_off_bottom.append((total_distance,con_highest))
                
                # Ordena o vetor com base na distância
                canditates_off_bottom = sorted(canditates_off_bottom)
                (total_distance,con_highest) = canditates_off_bottom[0]

                # Acha o retângulo correspondente ao contorno selecionado
#                blackbox = cv2.minAreaRect(contours_blk[con_highest])
            else:

                # Caso haja apenas um contorno esse é selecionado
                (y_highest,con_highest,x_min, y_min) = canditates[contours_blk_len-1]
#                blackbox = cv2.minAreaRect(contours_blk[con_highest])
            blackbox = cv2.minAreaRect(contours_blk[con_highest])
        (x_min, y_min), (w_min, h_min), ang = blackbox

        # Salva o ponto central para ser comparado no próximo frame
        x_last = x_min
        y_last = y_min

        # Configuração do número do ângulo
        if ang < -45 :
            ang = 90 + ang
        if w_min < h_min and ang > 0:
            ang = (90-ang)*-1
        if w_min > h_min and ang < 0:
            ang = 90 + ang

        # Mostra na tela a distancia (em px) horizontal da linha preta ao meio da imagem
        setpoint = 320
        error = int(x_min - setpoint)
        cv2.putText(image, str(error), (10, 320), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
        cv2.line(image, (int(x_min), 200 ), (int(x_min), 250 ), (255,0,0),3)

        # Mostra na tela o ângulo e a caixa
        box = np.int0(cv2.boxPoints(blackbox))
        cv2.drawContours(image,[box],0,(0,0,255),2)
        cv2.putText(image, str(int(ang)), (10, 40), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

    cv2.imshow("orginal with line", image)
    fps.setNewFrame()
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break

print(fps.finalFPS())
capture.release()
cv2.destroyAllWindows()
