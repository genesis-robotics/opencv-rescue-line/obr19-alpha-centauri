import pigpio
from H_Bridge import H_Bridge
import Adafruit_PCA9685
import time

L298N_MOTORS = {
    "right_pins": [19, 13],
    "left_pins": [6, 5],
    "right_pwm": 0,
    "left_pwm": 1
}

gpio = pigpio.pi()
pca = Adafruit_PCA9685.PCA9685()

pca.set_pwm_freq(100)

h_bridge = H_Bridge(L298N_MOTORS, gpio, pca)

print("Turn 90")
h_bridge.turn90("right")

time.sleep(2)

h_bridge.turn180("right")

gpio.stop()
