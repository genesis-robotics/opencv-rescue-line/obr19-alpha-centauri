from Digital_GPIO import Digital_GPIO
import pigpio
import time

gpio = pigpio.pi()
INPUT = 'input'
OUTPUT = 'output'

PIN_NUM = 4
INVERT = False
MODE = INPUT

io = Digital_GPIO(MODE, gpio, PIN_NUM, invert=INVERT)

try:
    while True:
        if MODE == INPUT:
            print(io.read())
            time.sleep(0.3)
        else:
            value = int(input("Valor: "))
            io.write(value)
except KeyboardInterrupt:
    if MODE == OUTPUT:
        io.write(0)
    print("Terminando...")
