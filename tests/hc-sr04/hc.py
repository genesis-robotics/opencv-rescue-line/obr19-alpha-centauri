import time
from UltrassonicSensor import UltrassonicSensor
import pigpio

io = pigpio.pi()
hc = UltrassonicSensor(io, 23, 24).start()

try:
	while True:
		distance = 0
		n_leituras = 0

		time.sleep(0.05)
		print(str(time.time()) + "  :  " + str(hc.distance))

except KeyboardInterrupt:
    pass
hc.stop()
io.stop()
