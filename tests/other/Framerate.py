import time


class Framerate:
    def __init__(self):
        self.prev_timestamp = time.time()
        self.start_timestamp = self.prev_timestamp
        self.n_frames = 0
        self.delta_time = 0
        self.previousIsStandby = True

    def getFPS(self):
        self.n_frames = self.n_frames + 1
        timestamp = time.time()
        freq = round(1/(timestamp - self.prev_timestamp))
        self.prev_timestamp = timestamp
        
        if self.previousIsStandby:
            self.previousIsStandby = False
            self.start_timestamp = time.time()
            return
        return int(freq)

    def setNewFrame(self):
        self.n_frames = self.n_frames + 1
        self.prev_timestamp = time.time()
        if self.previousIsStandby:
            self.previousIsStandby = False
            self.start_timestamp = time.time()

    def isStandby(self):
        if not self.previousIsStandby:
            self.previousIsStandby = True
            self.delta_time = self.delta_time + (time.time() - self.start_timestamp)

    def finalFPS(self):
        if not self.delta_time:
            self.delta_time = time.time() - self.start_timestamp
        return round(self.n_frames/self.delta_time, 2)
