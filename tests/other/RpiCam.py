import cv2
import time
from classes.RaspiCam import RaspiCam
from classes.Framerate import Framerate

capture = RaspiCam(640,480).start()
fps = Framerate()

last_frame = []

while True:
    frame = capture.getFrame(last_frame)
    if not len(frame):
        continue

    fps.setNewFrame()
    cv2.imshow("", frame)
    if cv2.waitKey(1) & 0xFF == ord("q"):
        break

print("FPS: "+str(fps.finalFPS()))