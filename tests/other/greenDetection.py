import time
import math
import cv2
import numpy as np
import tempo

# Cria a instância de captura das imagens da camera
CAPTURE = cv2.VideoCapture(0)

FPS = tempo.Framerate()

PX_DIVISOR = 1

FRAME_WIDTH = int(640 / PX_DIVISOR)
FRAME_HEIGHT = int(480 / PX_DIVISOR)

# Determina atributos da imagem
CAPTURE.set(cv2.CAP_PROP_FRAME_WIDTH, FRAME_WIDTH)  # Largura
CAPTURE.set(cv2.CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT)  # Altura
CAPTURE.set(cv2.CAP_PROP_FPS, 30)           # FPS
CAPTURE.set(cv2.CAP_PROP_BRIGHTNESS, 100)   # Luminosidade
CAPTURE.set(cv2.CAP_PROP_CONTRAST, 150)      # Contraste


HSV_MINIMUM = (0, 71, 0)
HSV_MAXIMUM = (87, 256, 256)
MINIMUM_GREEN_AREA = int(3000 / math.pow(PX_DIVISOR, 2))
GREEN_ITERATIONS_NUMBER = int(2 / PX_DIVISOR)
GREEN_ERODE_KERNEL = np.ones((3, 3), np.uint8)


def greenScan(image):

    hsv_image = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)

    thresh_image = cv2.inRange(hsv_image, HSV_MINIMUM, HSV_MAXIMUM)
    eroded_image = cv2.erode(
        thresh_image, GREEN_ERODE_KERNEL, iterations=GREEN_ITERATIONS_NUMBER)

    contours, _ = cv2.findContours(
        eroded_image, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)

    green_contours = []

    minimum_y = 0

    if len(contours):

        for i in range(len(contours)):
            moment = cv2.moments(contours[i])
            green_area = moment["m00"]

            if green_area > MINIMUM_GREEN_AREA:
                x = moment["m10"] / green_area
                y = moment["m01"] / green_area

                min_y = tuple(contours[i][contours[i][:, :, 1].argmax()][0])[1]
                if min_y > minimum_y:
                    minimum_y = min_y

                green_contours.append((green_area, x, y))

        green_contours = np.round(
            sorted(green_contours, reverse=True)).astype("int")

    if not len(green_contours):
        return None, None
    if len(green_contours) > 4:
        green_contours = green_contours[0:4]

    return green_contours, minimum_y


def drawTarget(x, y, color):
    cv2.circle(frame, (x, y), 10, color, 1)

    if(y-15 > 0):
        cv2.line(frame, (x, y), (x, y-15), color, 1)
    else:
        cv2.line(frame, (x, y), (x, 0), color, 1)
    if(y+15 < FRAME_HEIGHT):
        cv2.line(frame, (x, y), (x, y+15), color, 1)
    else:
        cv2.line(frame, (x, y), (x, FRAME_HEIGHT), color, 1)
    if(x-15 > 0):
        cv2.line(frame, (x, y), (x-15, y), color, 1)
    else:
        cv2.line(frame, (x, y), (0, y), color, 1)
    if(x+15 < FRAME_WIDTH):
        cv2.line(frame, (x, y), (x+15, y), color, 1)
    else:
        cv2.line(frame, (x, y), (FRAME_WIDTH, y), color, 1)


while True:
    _, frame = CAPTURE.read()

    GREEN_OBJECTS, lowest_y = greenScan(frame)

    if GREEN_OBJECTS is not None:
        for (_, x, y) in GREEN_OBJECTS:
            drawTarget(x, y, (0, 255, 0))

    else:
        pass

    cv2.imshow("", frame)
    FPS.setNewFrame()
    if cv2.waitKey(1) & 0xFF == ord("q"):
        break

print("FPS: "+str(FPS.finalFPS()))
CAPTURE.release()
cv2.destroyAllWindows()
