import pigpio
import time

FORWARD = [1900, 2200, 2500]
BACKWARD = [1100, 800, 500]


def map(x, in_min, in_max, out_min, out_max):
    return round((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min)


class Servo:
    def __init__(self, pigpio_obj, left_pin, right_pin):
        self.right_pin = right_pin
        self.left_pin = left_pin

        self.io = pigpio_obj

        self.io.set_mode(self.left_pin, pigpio.OUTPUT)
        self.io.set_mode(self.right_pin, pigpio.OUTPUT)

        self.stop()

    def stop(self):
        self.io.set_servo_pulsewidth(self.left_pin, 0)
        self.io.set_servo_pulsewidth(self.right_pin, 0)

    def run(self, left_value, right_value):
        if right_value > 0:
            right_value = FORWARD[right_value-1]
        elif right_value < 0:
            right_value = BACKWARD[right_value-1]
        else:
            right_value = 1500

        if left_value > 0:
            left_value = FORWARD[left_value-1]
        elif left_value < 0:
            left_value = BACKWARD[abs(left_value)-1]
        else:
            left_value = 1500

        self.io.set_servo_pulsewidth(self.right_pin, right_value)
        self.io.set_servo_pulsewidth(self.left_pin, left_value)

    def runCustom(self, left_value, right_value):
        self.io.set_servo_pulsewidth(self.right_pin, right_value)
        self.io.set_servo_pulsewidth(self.left_pin, left_value)


class Ultrasonicsensor:
    def __init__(self, pigpio_obj, trigger_pin, echo_pin):
        self.echo_pin = echo_pin
        self.trigger_pin = trigger_pin

        self.io = pigpio_obj

        self.io.set_mode(self.echo_pin, pigpio.INPUT)
        self.io.set_mode(self.trigger_pin, pigpio.OUTPUT)

    def measure(self, d_max):

        d_max = d_max / 17150

        pulse_start = None
        pulse_end = None

        self.io.write(self.trigger_pin, True)
        time.sleep(0.00001)
        self.io.write(self.trigger_pin, False)
        
        pulse_start = time.time()
        start_time = pulse_start
        while self.io.read(self.echo_pin) == 0:
            pulse_start = time.time()
            
            if pulse_start - start_time > d_max:
                return None
            
        if pulse_start is None:
            return None

        while self.io.read(self.echo_pin) == 1:
            pulse_end = time.time()
            
            if pulse_end - pulse_start > d_max:
                return None
            
        if pulse_end is None:
            return None

        return int(round((pulse_end - pulse_start) * 17150))
