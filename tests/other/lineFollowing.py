import time
import cv2
import numpy as np
import tempo
import math

# Cria a instância de captura das imagens da camera
capture = cv2.VideoCapture(0)

PX_DIVISOR = 1

FRAME_WIDTH = int(640 / PX_DIVISOR)
FRAME_HEIGHT = int(480 / PX_DIVISOR)

# Determina atributos da imagem
capture.set(cv2.CAP_PROP_FRAME_WIDTH, FRAME_WIDTH)  # Largura
capture.set(cv2.CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT)  # Altura
capture.set(cv2.CAP_PROP_FPS, 30)           # FPS
capture.set(cv2.CAP_PROP_BRIGHTNESS, 100)   # Luminosidade
capture.set(cv2.CAP_PROP_CONTRAST, 150)      # Contraste

BLACK_MAXIMUM_VALUE = 20
PIXEL_TOLERANCE = int(8 / PX_DIVISOR)
MINIMUM_LINE_WIDTH = int(52 / PX_DIVISOR)

NOT_FOUND_LIMIT = 10
NOT_FOUND_MULT_LIMIT = 4

FIRST_SCAN_Y = int(12 / PX_DIVISOR)
SCAN_Y_DELTA = int(4 / PX_DIVISOR)
SCAN_X_MAX_DELTA = int(24 / PX_DIVISOR)
FIRSTLINE_X_MAX_DELTA = int(120 / PX_DIVISOR)

BLACK_BGR_MINIMUM = (0, 0, 0)
BLACK_BGR_MAXIMUM = (12, 14, 23)
BLACK_ERODE_KERNEL = np.ones((3, 3), np.uint8)
BLACK_ERODE_ITERATIONS = int(2 / PX_DIVISOR)

fps = tempo.Framerate()

def resetPrevX():
    row_scan_array = []
    for i in range(int((FRAME_WIDTH - FIRST_SCAN_Y) / SCAN_Y_DELTA)):
        row_scan_array.append(int(FRAME_WIDTH / 2))
    return row_scan_array

PREV_X_ROW_SCAN = resetPrevX()

def rowBlackScan(row, comp_x):
    start = None
    end = None
    tolerance = 0
    lines = []
    foundLine = False
    
    for i in range(len(row)):
        if row[i]:
            if not start:
                start = i
            
            tolerance = PIXEL_TOLERANCE + 1
            end = i

        elif start and tolerance:
            tolerance = tolerance - 1

        if start and ((not tolerance and not row[i]) or i == (len(row)-1)):
            if end - start >= MINIMUM_LINE_WIDTH:
                centerX = int(round((start+end)/2))
                lines.append((abs(comp_x - centerX), centerX, end - start + 1))
                foundLine = True

            end = None
            start = None

    if foundLine:
        line = min(lines)
        return line[:][1:]
    else:
        return False, False
    
def getPointX (yP, x1 , y1, x2, y2):
    return int(round((x2*yP + x1*y2 - x1*yP - x2*y1)/(-y1 + y2)))

def getAngle(p0,p1):
    try:
        return int(round(math.degrees(math.atan((p1[0] - p0[0]) / (p0[1] - p1[1])))))
    except ZeroDivisionError:
        return 0
    
def scanInRows(image, min_y):
    NUM_SCANLINES = int((FRAME_HEIGHT - min_y - FIRST_SCAN_Y) / SCAN_Y_DELTA)

    blackline = cv2.inRange(image, BLACK_BGR_MINIMUM, BLACK_BGR_MAXIMUM)
    blackline = cv2.erode(blackline, BLACK_ERODE_KERNEL,
                          iterations=BLACK_ERODE_ITERATIONS)

    x = None
    firstLine = None
    lastLine = None
    notFoundAbs = 0
    notFound = 0
    this_x = []
    scan = True
        
    for i in range(NUM_SCANLINES):
        scan_Y = FRAME_HEIGHT - (i) * SCAN_Y_DELTA - FIRST_SCAN_Y
        
        if not scan:
            this_x.append((None, scan_Y))
            notFoundAbs = notFoundAbs + 1
            continue
        
        if not firstLine:
            x = PREV_X_ROW_SCAN[i]
            line_x, _ = rowBlackScan(blackline[scan_Y, x-FIRSTLINE_X_MAX_DELTA : x+FIRSTLINE_X_MAX_DELTA], FIRSTLINE_X_MAX_DELTA)
            if line_x:
                x = line_x + x - FIRSTLINE_X_MAX_DELTA
            
        else:
            delta_X = ((notFound if notFound < NOT_FOUND_MULT_LIMIT else NOT_FOUND_LIMIT) + 1) * SCAN_X_MAX_DELTA
            
            scan_X_min = x - delta_X
            
            if scan_X_min < 0:
                scan_X_min = 0
                
            scan_X_max = x + delta_X
            line_x, _ = rowBlackScan(blackline[scan_Y, scan_X_min:scan_X_max], PREV_X_ROW_SCAN[i] - scan_X_min)
            
            if line_x:
                x = line_x + scan_X_min
                
        
        if line_x:
            if not firstLine:
                firstLine = (x,scan_Y)
            this_x.append((x,scan_Y))
            notFound = 0
            lastLine = (x,scan_Y)
        else:
            this_x.append((None,scan_Y))
            notFound = notFound + 1
            if notFound > NOT_FOUND_LIMIT and scan_Y < int(FRAME_HEIGHT * 3/8):
                scan = False
            notFoundAbs = notFoundAbs + 1
            
    if notFoundAbs == NUM_SCANLINES - 1:
        for i in range(len(PREV_X_ROW_SCAN)):
            PREV_X_ROW_SCAN[i] = int(firstLine[0])
        pass
    elif notFoundAbs == NUM_SCANLINES:
        for i in range(len(PREV_X_ROW_SCAN)):
            PREV_X_ROW_SCAN[i] = int(FRAME_WIDTH/2)
    else:
        if notFoundAbs != 0:
            for i in range(NUM_SCANLINES):
                if this_x[i][0] is None:
                    this_x[i] = (getPointX(this_x[i][1], firstLine[0], firstLine[1], lastLine[0], lastLine[1]),this_x[i][1])
                PREV_X_ROW_SCAN[i] = int(round((this_x[i][0] + PREV_X_ROW_SCAN[i]) / 2))
    return firstLine, lastLine
    
while True:
    _, frame = capture.read()

    firstLine, lastLine = scanInRows(frame, 0)
    
    if firstLine is not None and lastLine is not None:
        frame = cv2.line(frame,firstLine,lastLine,(0,255,0),3)
                
        ang = getAngle(firstLine,lastLine)
        offcenter = int(round(((firstLine[0] + lastLine[0]) / 2) - FRAME_WIDTH/2))

        if firstLine[1] == FRAME_HEIGHT - FIRST_SCAN_Y:
            print("is gap")
        else:
            print("not gap")
        
        cv2.putText(frame,str(ang)+"deg", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,0),2)
        cv2.putText(frame,str(offcenter)+"px", (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,0),2)

    resetPrevX()
    cv2.imshow("", frame)
    fps.setNewFrame()
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break

print("FPS: "+str(fps.finalFPS()))
capture.release()
cv2.destroyAllWindows()
