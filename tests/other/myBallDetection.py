import numpy as np
import cv2
import math

FRAME_WIDTH = 640
FRAME_HEIGHT = 480

GAUSSIAN_BLUR_VALUE = 13    # 13 para 640/480
CIRCLE_OFFSET_LIMIT = 10    # Distancia limitae a ser considerada da bola ao centro

# Parametros para HoughCircles
CIRCLE_DP_PARAM = 1.3
CIRCLE_DBETWEEN_MIN = 40
CIRCLE_RADIUS_MIN = 0
CIRCLE_RADIUS_MAX = 0

capture = cv2.VideoCapture(0)
capture.set(3, FRAME_WIDTH)		# Largura
capture.set(4, FRAME_HEIGHT)		# Altura
capture.set(5, 30)		# FPS
capture.set(10, 100)  # Luminosidade

if __name__ == "__main__":
    #    _, frame = capture.read()
    frame = cv2.imread("fotos/circles/img.jpg")
    frame = cv2.resize(frame, (640, 480))

    output = frame.copy()

    gray = cv2.inRange(frame, (220, 220, 220), (255, 255, 255))

    gray = cv2.GaussianBlur(
        gray, (GAUSSIAN_BLUR_VALUE, GAUSSIAN_BLUR_VALUE), cv2.BORDER_DEFAULT)

#    gray = cv2.cvtColor(gray, cv2.COLOR_BGR2GRAY)

    circles = cv2.HoughCircles(
        gray, cv2.HOUGH_GRADIENT, CIRCLE_DP_PARAM, CIRCLE_DBETWEEN_MIN, minRadius=CIRCLE_RADIUS_MIN, maxRadius=CIRCLE_RADIUS_MAX)

    if circles is None:
        # VIRAR ROBO ATÉ ACHAR UM CÍRCULO
        pass
    else:
        circles = np.round(circles[0, :]).astype("int")

        print(circles)
        cv2.putText(output, str(len(circles)), (250, 470),
                    cv2.FONT_HERSHEY_SIMPLEX, 1.5, (0, 0, 0), 2)

        for (x, y, r) in circles:

            cv2.circle(output, (x, y), r, (0, 255, 0), 4)
            cv2.rectangle(output, (x - 5, y - 5),
                          (x + 5, y + 5), (0, 128, 255), -1)

    cv2.imshow("output", output)
    cv2.waitKey(0)

capture.release()
cv2.destroyAllWindows()
