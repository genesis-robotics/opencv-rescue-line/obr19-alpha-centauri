import time

class Framerate:
    def __init__(self):
        self.prev_timestamp = time.time()
        self.start_timestamp = self.prev_timestamp
        self.n_frames = 0
    
    def getFPS(self):
        timestamp = time.time()
        self.n_frames = self.n_frames + 1
        freq = round(1/(timestamp - self.prev_timestamp))
        self.prev_timestamp = timestamp
        return int(freq)
    
    def setNewFrame(self):
        self.n_frames = self.n_frames +1
    
    def finalFPS(self):
        return round(self.n_frames/(time.time() - self.start_timestamp),2)