import time

class CameraMovement:
    def __init__(self, servo_obj, servo_pin, servo_positions):
        self.servo_kit = servo_obj
        self.positions = servo_positions

        self.servo = servo_obj.servo[servo_pin]
        self.servo.set_pulse_width_range(500, 2400)

        self.down()

    def down(self):
        self.servo.angle = self.positions["down"]
        time.sleep(0.3)

    def up(self):
        self.servo.angle = self.positions["up"]
        time.sleep(0.3)


class RoboticArm:
    def __init__(self, servo_obj, servo_pins, servo_positions):
        self.servo_kit = servo_obj
        self.positions = servo_positions

        self.servo_claw = self.servo_kit.servo[servo_pins["claw"]]
        self.servo_wrist = self.servo_kit.servo[servo_pins["wrist"]]
        self.servo_shoulder = self.servo_kit.servo[servo_pins["shoulder"]]
        self.servo_box = self.servo_kit.servo[servo_pins["box"]]

        self.servo_claw.set_pulse_width_range(500, 2400)
        self.servo_wrist.set_pulse_width_range(500, 2400)
        self.servo_shoulder.set_pulse_width_range(500, 2500)
        self.servo_box.set_pulse_width_range(500, 2400)

        self.reset()

    def reset(self):
        self.claw_open()
        self.shoulder_down()
        self.wrist_back()
        self.box_down()

    def rescue_area(self):
        self.wrist_down()
        self.claw_open()
        self.shoulder_down()

    def grab_and_store(self):
        self.claw_close()
        time.sleep(0.5)

        self.shoulder_up()
        self.wrist_back()
        time.sleep(2)

        self.claw_open()
        time.sleep(0.5)

        self.rescue_area()
        self.box_up()
        time.sleep(3)

        self.box_down()

    def claw_open(self):
        self.servo_claw.angle = self.positions["claw"]["open"]

    def claw_close(self):
        self.servo_claw.angle = self.positions["claw"]["close"]

    def shoulder_down(self):
        self.servo_shoulder.angle = self.positions["claw"]["down"]

    def shoulder_up(self):
        self.servo_shoulder.angle = self.positions["claw"]["up"]

    def wrist_back(self):
        self.servo_wrist.angle = self.positions["wrist"]["back"]

    def wrist_down(self):
        self.servo_wrist.angle = self.positions["wrist"]["down"]

    def box_down(self):
        self.servo_box.angle = self.positions["box"]["down"]

    def box_up(self):
        self.servo_box.angle = self.positions["box"]["up"]
