from Servos import RoboticArm
from adafruit_servokit import ServoKit
from Adafruit_PCA9685 import PCA9685

PCA9685().pca.set_pwm_freq(100)
servo_kit = ServoKit(channels=16)

ARM_SERVO_PINS = {
    "claw": 2,
    "wrist": 3,
    "shoulder": 4,
    "box": 5
}
ARM_SERVO_POSITIONS = {
    "claw": {"open": 0, "close": 150},
    "wrist": {"down": 0, "back": 150},
    "shoulder": {"down": 0, "up": 150},
    "box": {"down": 0, "up": 150}
}

robotic_arm = RoboticArm(
                servo_kit, ARM_SERVO_PINS, ARM_SERVO_POSITIONS)